# 墨涩简单搜索导航（多款合一）

#### 介绍
墨涩简单搜索导航（多款合一）是基于开源简单搜索项目的美化版本，做了js文件、css文件本地化避免出错，简单的优化以及添加炫酷的多款背景：动态白云天空背景款“index.php”、动态海洋背景款“index1.php”、每日必应壁纸同步背景款“index2.php”，根据个人喜欢自行切换即可。只需要上传到web环境即可使用，可以作为日常浏览器主页使用！

#### 作者信息：
1. 作者：墨涩
2. QQ：651921384
3. 微信：mosetuifei
4. 网站：https://www.sunweihu.com/

#### 安装环境
将源码放到正常可用的web环境根目录就可以。

#### 文件结构图
- ![输入图片说明](https://images.gitee.com/uploads/images/2020/0211/110109_92fc84b2_4785166.jpeg "屏幕截图(411).jpg")

#### 多款导航模板切换方式
导航默认是以“index.php”文件为主页（除非你自己设置网站默认文件），动态白云天空背景款“index.php”、动态海洋背景款“index1.php”、每日必应壁纸同步背景款“index2.php”，如果你需要其他款式作为默认款式，只需要将“index1.php”或者“index2.php”的文件名称改为“index.php”即可切换。

#### 演示地址

1. 动态白云天空背景款“index.php”（默认是这款）
- [https://sou.sunweihu.com/](https://sou.sunweihu.com/)

2. 动态海洋背景款“index1.php”
- [https://sou.sunweihu.com/index1.php](https://sou.sunweihu.com/index1.php)

3. 每日必应壁纸同步背景款“index2.php”
- [https://sou.sunweihu.com/index2.php](https://sou.sunweihu.com/index2.php)

#### 导航截图
1. 动态白云天空背景款“index.php”（默认是这款）
- ![天空版](https://images.gitee.com/uploads/images/2020/0211/105435_2abab4a9_4785166.jpeg "2020011213013473.jpg")
- ![输入图片说明](https://images.gitee.com/uploads/images/2020/0211/105933_aa3bb73a_4785166.jpeg "2020011213013935.jpg")
2. 动态海洋背景款“index1.php”
- ![海洋](https://images.gitee.com/uploads/images/2020/0211/105508_ff4f283b_4785166.jpeg "2020011213014048.jpg")
- ![输入图片说明](https://images.gitee.com/uploads/images/2020/0211/105919_a1603fe4_4785166.jpeg "2020011213014297.jpg")
3. 每日必应壁纸同步背景款“index2.php”
- ![输入图片说明](https://images.gitee.com/uploads/images/2020/0211/105825_6de7954d_4785166.jpeg "屏幕截图(410).jpg")
- ![输入图片说明](https://images.gitee.com/uploads/images/2020/0211/105836_d4816374_4785166.jpeg "屏幕截图(409).jpg")

#### 组件：

 **图标：** 
图标调用了阿里的图标https://www.iconfont.cn/，

 **天气组件** 
天气组件为心知天气插件
