<?php
//禁用错误报告
error_reporting(0);
$t=htmlspecialchars($_POST["t"]);
$q=htmlspecialchars($_POST["q"]);
if ($q==""||$q==null) {
}else{
  if ($t=="b"){
    echo'<script>window.open("//www.baidu.com/s?ie=utf-8&word='.$q.'","_blank");</script>';
  }else if($t=="g"){
    echo'<script>window.open("https://g.louqunhua.top/search?hl=zh&q='.$q.'","_blank");</script>';
  }else{
    //默认百度
    echo'<script>window.open("https://www.baidu.com/s?ie=utf-8&word='.$q.'","_blank");</script>';
  }
};
?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="Cache-Control" content="no-siteapp">
  <link rel="icon" href="icon/192.png" sizes="192x192" />
  <link rel="apple-touch-icon-precomposed" href="icon/300.png" />
  <meta name="msapplication-TileImage" content="icon/300.png" />
  <link rel="shortcut icon" href="icon/32.png"/>
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-touch-fullscreen" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="full-screen" content="yes"><!--UC强制全屏-->
  <meta name="browsermode" content="application"><!--UC应用模式-->
  <meta name="x5-fullscreen" content="true"><!--QQ强制全屏-->
  <meta name="x5-page-mode" content="app"><!--QQ应用模式-->
  <title>墨涩搜索——墨涩网</title>
  <link href="css/style.css?t=<?php echo date("ymdhi"); ?>" rel="stylesheet">
  <link rel="stylesheet" href="//at.alicdn.com/t/font_1621716_zejv1a0y54m.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/sou.js?t=<?php echo date("ymdhi"); ?>"></script>
  <!--动态背景天空开始。-->
<style>
body {
  overflow: hidden; 
  margin: 0; 
}</style>
<!--动态背景天空结束。-->
</head>

<body>
    <div id="menu"><i></i></div>
    <div class="list closed">
        <ul>
          <!------>
            <li class="title"><i class="iconfont iconm1"></i>墨涩网服务</li>
            <li><a rel="nofollow" href="https://www.sunweihu.com/" target="_blank"><i class="iconfont iconm" style="color: #fe0000;"></i>墨涩网</a></li>
            <li><a rel="nofollow" href="https://wiki.sunweihu.com/" target="_blank"><i class="iconfont iconshuxuexi-" style="color: #ff4c01;"></i>个人wiki</a></li>
            <li><a rel="nofollow" href="https://windows.sunweihu.com" target="_blank"><i class="iconfont icondiannao" style="color: #ff6600;"></i>云桌面</a></li>
            <li><a rel="nofollow" href="https://www.sunweihu.com/Bizhi/" target="_blank"><i class="iconfont iconpicture" style="color: #ffa600;"></i>墨涩壁纸</a></li>
            <li><a rel="nofollow" href="https://ps.sunweihu.com/" target="_blank"><i class="iconfont iconps" style="color: #e6162d;"></i>在线PS</a></li>
            <li><a rel="nofollow" href="https://jiexi.sunweihu.com/" target="_blank"><i class="iconfont iconvip_icon_al" style="color: #73ff00;"></i>VIP解析</a></li>
            <li><a rel="nofollow" href="https://wangzhi.sunweihu.com/" target="_blank"><i class="iconfont iconduan-copy" style="color: #f15534;"></i>短网址</a></li>
            <li><a rel="nofollow" href="https://fm.sunweihu.com" target="_blank"><i class="iconfont iconFM" style="color: #0166ff;"></i>在线FM</a></li>
            <li><a rel="nofollow" href="https://tools.sunweihu.com/" target="_blank"><i class="iconfont icongongjuxiang" style="color: #00ff7f;"></i>工具箱</a></li>
            <li><a rel="nofollow" href="https://www.sunweihu.com/sou-music/" target="_blank"><i class="iconfont iconyinyue" style="color: #e6162d;"></i>音乐下载</a></li>
            <li><a rel="nofollow" href="https://www.sunweihu.com/music/" target="_blank"><i class="iconfont iconyinle" style="color: #0166ff;"></i>在线音乐</a></li>
            <li><a rel="nofollow" href="https://bing.sunweihu.com/" target="_blank"><i class="iconfont iconbizhixuanzhong" style="color: #120afb;"></i>必应壁纸盘</a></l>
            <li><a rel="nofollow" href="https://api.sunweihu.com/" target="_blank"><i class="iconfont icongongjuxiang" style="color: #a329cc;"></i>墨涩APi</a></l>
            
            <!------>
            <li class="title"><i class="iconfont icongongju"></i>工具箱</li>
            <li><a rel="nofollow" href="https://www.uzer.me/" target="_blank"><i class="iconfont iconzaixian" style="color: #fe0000;"></i>Uzer.me</a></li>
            <li><a rel="nofollow" href="http://www.peiyinge.com/" target="_blank"><i class="iconfont iconseo" style="color: #ff4c01;"></i>配音阁</a></li>
            <li><a rel="nofollow" href="https://www.baiduwp.com" target="_blank"><i class="iconfont iconpan" style="color: #ff6600;"></i>网盘解析</a></li>
            <li><a rel="nofollow" href="https://www.mdeditor.com/" target="_blank"><i class="iconfont iconiconset0122" style="color:#ffa600;"></i>MD编辑器</a></li>
            <li><a rel="nofollow" href="https://www.iamwawa.cn/" target="_blank"><i class="iconfont icongongjuxiang1" style="color:#73ff00;"></i>蛙蛙工具箱</a></li>
            <li><a rel="nofollow" href="https://tool.wpjam.com/" target="_blank"><i class="iconfont icongongjuxiang1" style="color:#00ff01;"></i>山炮工具箱</a></li>
            <li><a rel="nofollow" href="http://www.gjw123.com/" target="_blank"><i class="iconfont icongongjuxiang1" style="color:#00ff7f;"></i>123工具箱</a></li>
            <li><a rel="nofollow" href="https://www.toolfk.com/" target="_blank"><i class="iconfont icongongjuxiang1" style="color:#120afb;"></i>FK工具箱</a></li>
            <li><a rel="nofollow" href="https://fakeupdate.net/" target="_blank"><i class="iconfont iconshengji" style="color:#a329cc;"></i>假装升级</a></li>
            <li><a rel="nofollow" href="https://gitmind.cn/" target="_blank"><i class="iconfont iconluojituboth" style="color:#8c08f1;"></i>思维导图</a></li>
            <li><a rel="nofollow" href="https://bigjpg.com/" target="_blank"><i class="iconfont iconziyuan" style="color:#148bfe;"></i>图片放大</a></li>
            <li><a rel="nofollow" href="https://jianwai.netease.com/" target="_blank"><i class="iconfont iconShapew" style="color:#FF0000;"></i>见外工作台</a></li>
            <li><a rel="nofollow" href="http://tool.uixsj.cn/" target="_blank"><i class="iconfont icongongjuxiang1" style="color:#FFFF00;"></i>现实工具箱</a></li>
            <!------>
            <li class="title"><i class="iconfont icondianying1"></i>影视资源</li>
            <li><a rel="nofollow" href="http://ifkdy.com/" target="_blank"><i class="iconfont icondianying" style="color:#fe0000;"></i>疯狂影视</a></li>
            <li><a rel="nofollow" href="https://www.cupfox.com/" target="_blank"><i class="iconfont icondianying" style="color:#ff4c01;"></i>茶杯狐</a></li>
            <li><a rel="nofollow" href="https://www.dybee.tv/" target="_blank"><i class="iconfont icondianying" style="color:#ff6600;"></i>电影蜜蜂</a></li>
            <li><a rel="nofollow" href="http://yyetss.com/" target="_blank"><i class="iconfont icondianying" style="color:#ffa600;"></i>人人影视</a></li>
            <li><a rel="nofollow" href="http://www.zmz2019.com/" target="_blank"><i class="iconfont icondianying" style="color: #73ff00;"></i>字幕组</a></li>
            <li><a rel="nofollow" href="https://www.66e.cc/index.htm" target="_blank"><i class="iconfont icondianying" style="color:#00ff01;"></i>66影视</a></li>
            <li><a rel="nofollow" href="https://www.xiaopian.com/" target="_blank"><i class="iconfont icondianying" style="color:#00ff7f;"></i>电影天堂</a></li>
            <li><a rel="nofollow" href="http://www.anzhuotan.com/" target="_blank"><i class="iconfont icondianying" style="color:#0166ff;"></i>追美剧</a></li>
            <li><a rel="nofollow" href="https://www.03pjpj.net/" target="_blank"><i class="iconfont icondianying" style="color:#120afb;"></i>普京影视</a></li>
            <li><a rel="nofollow" href="http://mcar.co/forum.php" target="_blank"><i class="iconfont icondianying" style="color:#a329cc;"></i>耐卡影音</a></li>
            <li><a rel="nofollow" href="http://www.f8dy.tv/" target="_blank"><i class="iconfont icondianying" style="color: #8c08f1;"></i>F8电影</a></li>
            <li><a rel="nofollow" href="https://www.66e.cc/index.htm" target="_blank"><i class="iconfont icondianying" style="color:#f33;"></i>66影视</a></li>
            <li><a rel="nofollow" href="https://www.xiaopian.com/" target="_blank"><i class="iconfont icondianying" style="color:#148bfe;"></i>电影天堂</a></li>
            <li><a rel="nofollow" href="http://www.anzhuotan.com/" target="_blank"><i class="iconfont icondianying" style="color:#fe0000;"></i>追美剧</a></li>
            <!------>
            <li class="title"><i class="iconfont iconxinmeiti"></i>新媒体平台</li>
            <li><a rel="nofollow" href="https://www.weibo.com" target="_blank"><i class="iconfont iconxinwen" style="color: #fe0000;"></i>微博</a></li>
            <li><a rel="nofollow" href="https://www.zhihu.com/" target="_blank"><i class="iconfont iconxinwen" style="color: #ff4c01;"></i>知乎</a></li>
            <li><a rel="nofollow" href="https://www.douban.com/" target="_blank"><i class="iconfont iconxinwen" style="color: #ff6600;"></i>豆瓣</a></li>
            <li><a rel="nofollow" href="https://mp.weixin.qq.com/" target="_blank"><i class="iconfont iconxinwen" style="color: #ffa600;"></i>微信公众号</a></li>
            <li><a rel="nofollow" href="https://mp.dayu.com/" target="_blank"><i class="iconfont iconxinwen" style="color: #73ff00;"></i>大鱼号</a></li>
            <li><a rel="nofollow" href="https://mp.toutiao.com/" target="_blank"><i class="iconfont iconxinwen" style="color: #73ff00;"></i>头条号</a></li>
            <li><a rel="nofollow" href="https://om.qq.com/" target="_blank"><i class="iconfont iconxinwen" style="color: #00ff01;"></i>企鹅号</a></li>
            <li><a rel="nofollow" href="https://baijiahao.baidu.com/" target="_blank"><i class="iconfont iconxinwen" style="color: #00ff7f;"></i>百家号</a></li>
            <li><a rel="nofollow" href="http://mp.163.com/wemedia/login.html" target="_blank"><i class="iconfont iconxinwen" style="color: #0166ff;"></i>网易号</a></li>
            <li><a rel="nofollow" href="https://www.jianshu.com/" target="_blank"><i class="iconfont iconxinwen" style="color: #120afb;"></i>简书</a></li>
            <li><a rel="nofollow" href="http://mp.sina.com.cn/" target="_blank"><i class="iconfont iconxinwen" style="color: #a329cc;"></i>新浪看点</a></li>
            <li><a rel="nofollow" href="https://www.douyin.com/" target="_blank"><i class="iconfont iconxinwen" style="color: #8c08f1;"></i>抖音</a></li>
            <li><a rel="nofollow" href="https://www.kuaishou.com/" target="_blank"><i class="iconfont iconxinwen" style="color: #148bfe;"></i>快手</a></li>
          <!------>
            <li class="title"><i class="iconfont iconwangpan1"></i>网盘资源</li>
            <li><a rel="nofollow" href="https://pan.baidu.com" target="_blank"><i class="iconfont iconwangpan" style="color: #fe0000;"></i>百度网盘</a></li>
            <li><a rel="nofollow" href="https://www.weiyun.com/" target="_blank"><i class="iconfont iconwangpan" style="color: #ff4c01;"></i>腾讯微云</a></li>
            <li><a rel="nofollow" href="https://yunpan.360.cn/" target="_blank"><i class="iconfont iconwangpan" style="color: #ff6600;"></i>360云盘</a></li>
            <li><a rel="nofollow" href="https://cloud.189.cn/" target="_blank"><i class="iconfont iconwangpan" style="color: #ffa600;"></i>天翼云盘</a></li>
            <li><a rel="nofollow" href="https://www.ctfile.com/" target="_blank"><i class="iconfont iconwangpan" style="color: #ffe51a;"></i>城通网盘</a></li>
            <li><a rel="nofollow" href="https://www.jianguoyun.com/" target="_blank"><i class="iconfont iconwangpan" style="color: #73ff00;"></i>坚果云</a></li>
            <li><a rel="nofollow" href="http://www.lanzou.com/" target="_blank"><i class="iconfont iconwangpan" style="color: #00ff01;"></i>蓝奏云</a></li>
            <li><a rel="nofollow" href="https://cowtransfer.com/" target="_blank"><i class="iconfont iconwangpan" style="color: #00ff7f;"></i>奶牛快传</a></li>
            <li><a rel="nofollow" href="https://www.quqi.com/" target="_blank"><i class="iconfont iconwangpan" style="color: #0166ff;"></i>曲奇云盘</a></li>
            <li><a rel="nofollow" href="https://onedrive.live.com/" target="_blank"><i class="iconfont iconwangpan" style="color: #120afb;"></i>onedrive</a></li>
            <li><a rel="nofollow" href="http://u-file.cn/" target="_blank"><i class="iconfont iconwangpan" style="color: #a329cc;"></i>u-file</a></li>
            <li><a rel="nofollow" href="https://airportal.cn/" target="_blank"><i class="iconfont iconwangpan" style="color: #8c08f1;"></i>空投</a></li>
            <li><a rel="nofollow" href="https://send.firefox.com/" target="_blank"><i class="iconfont iconwangpan" style="color: #148bfe;"></i>火狐Send</a></li>
          <!------>
            <li class="title"><i class="iconfont iconyouxiang1"></i> 邮箱</li>
            <li><a rel="nofollow" href="https://mail.google.com/mail/u/0/#inbox" target="_blank"><i class="iconfont iconyouxiang" style="color: #fe0000;"></i>Gmail</a></li>
            <li><a rel="nofollow" href="https://outlook.live.com/mail/" target="_blank"><i class="iconfont iconyouxiang" style="color: #ff4c01;"></i>Hotmail</a></li>
            <li><a rel="nofollow" href="https://mail.163.com/" target="_blank"><i class="iconfont iconyouxiang" style="color: #ff6600;"></i>网易邮箱</a></li>
            <li><a rel="nofollow" href="https://mail.sina.com.cn/" target="_blank"><i class="iconfont iconyouxiang" style="color: #ffa600;"></i>新浪邮箱</a></li>
            <li><a rel="nofollow" href="https://mail.qq.com/" target="_blank"><i class="iconfont iconyouxiang" style="color: #ffe51a;"></i>QQ邮箱</a></li>
            <li><a rel="nofollow" href="https://qiye.aliyun.com/" target="_blank"><i class="iconfont iconyouxiang" style="color: #73ff00;"></i>阿里邮箱</a></li>
          <!------>
            <li class="title"><i class="iconfont icongouwu1"></i> 购物</li>
            <li><a rel="nofollow" href="https://www.taobao.com" target="_blank"><i class="iconfont icongouwu" style="color: #fe0000;"></i>淘宝网</a></li>
            <li><a rel="nofollow" href="https://dyartstyle.com/juhuasuan/" target="_blank"><i class="iconfont icongouwu" style="color: #ff4c01;"></i>聚划算</a></li>
            <li><a rel="nofollow" href="https://dyartstyle.com/temai/" target="_blank"><i class="iconfont icongouwu" style="color: #ff6600;"></i>淘宝特卖</a></li>
            <li><a rel="nofollow" href="https://mobile.yangkeduo.com/"  target="_blank"><i class="iconfont icongouwu" style="color: #ffa600;"></i>拼多多</a></li>
            <li><a rel="nofollow" href="https://www.jd.com" target="_blank"><i class="iconfont icongouwu" style="color: #ffe51a;"></i>京东</a></li>
            <li><a rel="nofollow" href="https://www.suning.com/" target="_blank"><i class="iconfont icongouwu" style="color: #73ff00;"></i>苏宁易购</a></li>
            <li><a rel="nofollow" href="http://you.163.com/" target="_blank"><i class="iconfont icongouwu" style="color: #00ff01;"></i>网易严选</a></li>
            <li><a rel="nofollow" href="https://www.amazon.cn/" target="_blank"><i class="iconfont icongouwu" style="color: #00ff7f;"></i>亚马逊</a></li>
            <li><a rel="nofollow" href="http://www.dangdang.com/" target="_blank"><i class="iconfont icongouwu" style="color: #0166ff;"></i>当当</a></li>
          <!------>
            <li class="title"><i class="iconfont iconicon-test"></i> 设计视觉</li>
            <li><a rel="nofollow" href="http://www.58pic.com/" target="_blank"><i class="iconfont iconsheji" style="color:#0aa;"></i>千图网</a></li>
            <li><a rel="nofollow" href="http://www.nipic.com/" target="_blank"><i class="iconfont iconsheji"></i>昵图网</a></li>
            <li><a rel="nofollow" href="http://www.lanrentuku.com/" target="_blank"><i class="iconfont iconsheji" style="color:#e02;"></i>懒人图库</a></li>
            <li><a rel="nofollow" href="https://www.ooopic.com/" target="_blank"><i class="iconfont iconsheji" style="color:#56f;"></i>我图网</a></li>
            <li><a rel="nofollow" href="https://stock.tuchong.com/" target="_blank"><i class="iconfont iconsheji" style="color:#e48;"></i>图虫</a></li>
            <li><a rel="nofollow" href="https://huaban.com/" target="_blank"><i class="iconfont iconsheji" style="color:#f06;"></i>花瓣</a></li>
            <li><a rel="nofollow" href="https://www.zcool.com.cn/" target="_blank"><i class="iconfont iconsheji" style="color:#f90;"></i>站酷</a></li>
            <li><a rel="nofollow" href="https://www.iconfont.cn/" target="_blank"><i class="iconfont iconsheji" style="color: #ff6019;"></i>阿里图标</a></li>
            <li><a rel="nofollow" href="https://www.iconfinder.com/" target="_blank"><i class="iconfont iconsheji"></i>IconFinder</a></li>
            <li><a rel="nofollow" href="https://588ku.com/" target="_blank"><i class="iconfont iconsheji" style="color:#0aa;"></i>千库网</a></li>
            <!------>
            <li class="title"><i class="iconfont iconzhanchang"></i> 站长工具</li>
            <li><a rel="nofollow" href="https://ping.chinaz.com/" target="_blank"><i class="iconfont iconwangzhan" style="color:#FF0000;"></i>站长Ping</a></li>
            <li><a rel="nofollow" href="http://ip.tool.chinaz.com/" target="_blank"><i class="iconfont iconwangzhan" style="color: #FFFF00;"></i>IP查询</a></li>
            <li><a rel="nofollow" href="http://seo.chinaz.com/" target="_blank"><i class="iconfont iconwangzhan" style="color: #00FF00;"></i>SEO查询</a></li>
            <li><a rel="nofollow" href="http://www.w3school.com.cn/" target="_blank"><i class="iconfont iconwangzhan" style="color:#00FFFF;"></i>W3school</a></li>
            <li><a rel="nofollow" href="https://github.com/" target="_blank"><i class="iconfont iconwangzhan" style="color: #0000FF;"></i>Github</a></li>
            <li><a rel="nofollow" href="https://coding.net/" target="_blank"><i class="iconfont iconwangzhan"style="color: #FF00FF;"></i>Coding</a></li>
            <li><a rel="nofollow" href="https://freessl.cn/" target="_blank"><i class="iconfont iconwangzhan"style="color: #800000;"></i>FreeSSL</a></li>
            <li><a rel="nofollow" href="https://www.52pojie.cn/" target="_blank"><i class="iconfont iconwangzhan" style="color:#808000;"></i>吾爱破解</a></li>
            <li><a rel="nofollow" href="https://segmentfault.com/" target="_blank"><i class="iconfont iconwangzhan" style="color:#008000;"></i>SF思否</a></li>
            <li><a rel="nofollow" href="https://www.qiniu.com/" target="_blank"><i class="iconfont iconwangzhan" style="color: #008080;"></i>七牛云</a></li>
            <li><a rel="nofollow" href="https://www.upyun.com/" target="_blank"><i class="iconfont iconwangzhan" style="color: #000080;"></i>又拍云</a></li>
            <li><a rel="nofollow" href="https://msdn.itellyou.cn/" target="_blank"><i class="iconfont iconwangzhan" style="color: #800080;"></i>MSDN下载</a></li>
            <li><a rel="nofollow" href="https://www.ucloud.cn/" target="_blank"><i class="iconfont iconwangzhan" style="color: #FF0000;"></i>Ucloud</a></li>
            <li><a rel="nofollow" href="https://www.bt.cn/" target="_blank"><i class="iconfont iconwangzhan" style="color: #FFFF00;"></i>宝塔面板</a></li>
            <li><a rel="nofollow" href="https://www.xp.cn/" target="_blank"><i class="iconfont iconwangzhan" style="color: #00FF00;"></i>PhpStudy</a></li>
            <li><a rel="nofollow" href="https://ziyuan.baidu.com/" target="_blank"><i class="iconfont iconwangzhan" style="color: #00FFFF;"></i>百度站长</a></li>
            <li><a rel="nofollow" href="http://zhanzhang.so.com/" target="_blank"><i class="iconfont iconwangzhan" style="color: #0000FF;"></i>360站长</a></li>
            <li><a rel="nofollow" href="http://zhanzhang.sogou.com/" target="_blank"><i class="iconfont iconwangzhan" style="color: #FF00FF;"></i>搜狗站长</a></li>
            <li><a rel="nofollow" href="http://www.google.cn/webmasters" target="_blank"><i class="iconfont iconwangzhan" style="color: #FF00FF;"></i>谷歌站长</a></li>
            <li><a rel="nofollow" href="https://www.aliyun.com/" target="_blank"><i class="iconfont iconwangzhan" style="color:#800000;"></i>阿里云</a></li>
            <li><a rel="nofollow" href="https://cloud.tencent.com/" target="_blank"><i class="iconfont iconwangzhan" style="color:#808000;"></i>腾讯云</a></li>
            <li><a rel="nofollow" href="https://cloud.baidu.com/" target="_blank"><i class="iconfont iconwangzhan" style="color:#008000;"></i>百度云</a></li>
        </ul>
    </div>
    <div id="tp-weather-widget" class="mywth"></div>
  <script>
    (function(a,h,g,f,e,d,c,b){b=function(){d=h.createElement(g);c=h.getElementsByTagName(g)[0];d.src=e;d.charset="utf-8";d.async=1;c.parentNode.insertBefore(d,c)};a["SeniverseWeatherWidgetObject"]=f;a[f]||(a[f]=function(){(a[f].q=a[f].q||[]).push(arguments)});a[f].l=+new Date();if(a.attachEvent){a.attachEvent("onload",b)}else{a.addEventListener("load",b,false)}}(window,document,"script","SeniverseWeatherWidget","//cdn.sencdn.com/widget2/static/js/bundle.js?t="+parseInt((new Date().getTime() / 100000000).toString(),10)));
    window.SeniverseWeatherWidget('show', {
      flavor: "slim",
      location: "WX4FBXXFKE4F",
      geolocation: true,
      language: "zh-Hans",
      unit: "c",
      theme: "auto",
      token: "fd08684d-bf66-4982-999d-4d0f260c236d",
      hover: "enabled",
      container: "tp-weather-widget"
    })
  </script>
  

  <!--动态背景天空开始。-->
<script src="js/three.min.js"></script>

<div id="container"></div>

<script id="vertex-shader" type="no-js">
  void main()	{
    gl_Position = vec4( position, 1.0 );
  }
</script>
<script id="fragment-shader" type="no-js">
  uniform float iGlobalTime;
  uniform vec2 iResolution;

  const int NUM_STEPS = 8;
  const float PI	 	= 3.1415;
  const float EPSILON	= 1e-3;
  float EPSILON_NRM	= 0.1 / iResolution.x;

  // sea variables
  const int ITER_GEOMETRY = 3;
  const int ITER_FRAGMENT = 5;
  const float SEA_HEIGHT = 0.6;
  const float SEA_CHOPPY = 1.0;
  const float SEA_SPEED = 1.0;
  const float SEA_FREQ = 0.16;
  const vec3 SEA_BASE = vec3(0.1,0.19,0.22);
  const vec3 SEA_WATER_COLOR = vec3(0.8,0.9,0.6);
  float SEA_TIME = iGlobalTime * SEA_SPEED;
  mat2 octave_m = mat2(1.6,1.2,-1.2,1.6);

  mat3 fromEuler(vec3 ang) {
    vec2 a1 = vec2(sin(ang.x),cos(ang.x));
    vec2 a2 = vec2(sin(ang.y),cos(ang.y));
    vec2 a3 = vec2(sin(ang.z),cos(ang.z));
    mat3 m;
    m[0] = vec3(
    	a1.y*a3.y+a1.x*a2.x*a3.x,
    	a1.y*a2.x*a3.x+a3.y*a1.x,
    	-a2.y*a3.x
    );
    m[1] = vec3(-a2.y*a1.x,a1.y*a2.y,a2.x);
    m[2] = vec3(
    	a3.y*a1.x*a2.x+a1.y*a3.x,
      a1.x*a3.x-a1.y*a3.y*a2.x,
      a2.y*a3.y
    );
    return m;
  }

  float hash( vec2 p ) {
    float h = dot(p,vec2(127.1,311.7));	
    return fract(sin(h)*43758.5453123);
  }

  float noise( in vec2 p ) {
    vec2 i = floor(p);
    vec2 f = fract(p);	
    vec2 u = f * f * (3.0 - 2.0 * f);
    return -1.0 + 2.0 * mix(
    	mix(
      	hash(i + vec2(0.0,0.0)
      ), 
    	hash(i + vec2(1.0,0.0)), u.x),
    	mix(hash(i + vec2(0.0,1.0) ), 
    	hash(i + vec2(1.0,1.0) ), u.x), 
      u.y
    );
  }

  float diffuse(vec3 n,vec3 l,float p) {
    return pow(dot(n,l) * 0.4 + 0.6,p);
  }

  float specular(vec3 n,vec3 l,vec3 e,float s) {    
    float nrm = (s + 8.0) / (3.1415 * 8.0);
    return pow(max(dot(reflect(e,n),l),0.0),s) * nrm;
  }

  vec3 getSkyColor(vec3 e) {
    e.y = max(e.y, 0.0);
    vec3 ret;
    ret.x = pow(1.0 - e.y, 2.0);
    ret.y = 1.0 - e.y;
    ret.z = 0.6+(1.0 - e.y) * 0.4;
    return ret;
  }


  float sea_octave(vec2 uv, float choppy) {
    uv += noise(uv);         
    vec2 wv = 1.0 - abs(sin(uv));
    vec2 swv = abs(cos(uv));    
    wv = mix(wv, swv, wv);
    return pow(1.0 - pow(wv.x * wv.y, 0.65), choppy);
  }

  float map(vec3 p) {
    float freq = SEA_FREQ;
    float amp = SEA_HEIGHT;
    float choppy = SEA_CHOPPY;
    vec2 uv = p.xz; 
    uv.x *= 0.75;

    float d, h = 0.0;    
    for(int i = 0; i < ITER_GEOMETRY; i++) {        
      d = sea_octave((uv + SEA_TIME) * freq, choppy);
      d += sea_octave((uv - SEA_TIME) * freq, choppy);
      h += d * amp;        
      uv *= octave_m;
      freq *= 1.9; 
      amp *= 0.22;
      choppy = mix(choppy, 1.0, 0.2);
    }
    return p.y - h;
  }

  float map_detailed(vec3 p) {
      float freq = SEA_FREQ;
      float amp = SEA_HEIGHT;
      float choppy = SEA_CHOPPY;
      vec2 uv = p.xz;
      uv.x *= 0.75;

      float d, h = 0.0;    
      for(int i = 0; i < ITER_FRAGMENT; i++) {        
        d = sea_octave((uv+SEA_TIME) * freq, choppy);
        d += sea_octave((uv-SEA_TIME) * freq, choppy);
        h += d * amp;        
        uv *= octave_m;
        freq *= 1.9; 
        amp *= 0.22;
        choppy = mix(choppy,1.0,0.2);
      }
      return p.y - h;
  }

  vec3 getSeaColor(
  	vec3 p,
    vec3 n, 
    vec3 l, 
    vec3 eye, 
    vec3 dist
  ) {  
    float fresnel = 1.0 - max(dot(n,-eye),0.0);
    fresnel = pow(fresnel,3.0) * 0.65;

    vec3 reflected = getSkyColor(reflect(eye,n));    
    vec3 refracted = SEA_BASE + diffuse(n,l,80.0) * SEA_WATER_COLOR * 0.12; 

    vec3 color = mix(refracted,reflected,fresnel);

    float atten = max(1.0 - dot(dist,dist) * 0.001, 0.0);
    color += SEA_WATER_COLOR * (p.y - SEA_HEIGHT) * 0.18 * atten;

    color += vec3(specular(n,l,eye,60.0));

    return color;
  }

  // tracing
  vec3 getNormal(vec3 p, float eps) {
    vec3 n;
    n.y = map_detailed(p);    
    n.x = map_detailed(vec3(p.x+eps,p.y,p.z)) - n.y;
    n.z = map_detailed(vec3(p.x,p.y,p.z+eps)) - n.y;
    n.y = eps;
    return normalize(n);
  }

  float heightMapTracing(vec3 ori, vec3 dir, out vec3 p) {  
    float tm = 0.0;
    float tx = 1000.0;    
    float hx = map(ori + dir * tx);

    if(hx > 0.0) {
      return tx;   
    }

    float hm = map(ori + dir * tm);    
    float tmid = 0.0;
    for(int i = 0; i < NUM_STEPS; i++) {
      tmid = mix(tm,tx, hm/(hm-hx));                   
      p = ori + dir * tmid;                   
      float hmid = map(p);
      if(hmid < 0.0) {
        tx = tmid;
        hx = hmid;
      } else {
        tm = tmid;
        hm = hmid;
       }
    }
    return tmid;
  }

  void main() {
    vec2 uv = gl_FragCoord.xy / iResolution.xy;
    uv = uv * 2.0 - 1.0;
    uv.x *= iResolution.x / iResolution.y;    
    float time = iGlobalTime * 0.3;

    // ray
    vec3 ang = vec3(
      sin(time*3.0)*0.1,sin(time)*0.2+0.3,time
    );    
    vec3 ori = vec3(0.0,3.5,time*5.0);
    vec3 dir = normalize(
      vec3(uv.xy,-2.0)
    );
    dir.z += length(uv) * 0.15;
    dir = normalize(dir);

    // tracing
    vec3 p;
    heightMapTracing(ori,dir,p);
    vec3 dist = p - ori;
    vec3 n = getNormal(
      p,
      dot(dist,dist) * EPSILON_NRM
    );
    vec3 light = normalize(vec3(0.0,1.0,0.8)); 

    // color
    vec3 color = mix(
      getSkyColor(dir),
      getSeaColor(p,n,light,dir,dist),
      pow(smoothstep(0.0,-0.05,dir.y),0.3)
    );

    // post
    gl_FragColor = vec4(pow(color,vec3(0.75)), 1.0);
  }
</script>

<script>
var container, 
    renderer, 
    scene, 
    camera, 
    mesh, 
    start = Date.now(),
    fov = 30;

var clock = new THREE.Clock();

var timeUniform = {
	iGlobalTime: {
		type: 'f',
		value: 0.1
	},
	iResolution: {
		type: 'v2',
		value: new THREE.Vector2()
	}
};

timeUniform.iResolution.value.x = window.innerWidth;
timeUniform.iResolution.value.y = window.innerHeight;

window.addEventListener('load', function() {
  container = document.getElementById('container');
  scene = new THREE.Scene();
  
  camera = new THREE.PerspectiveCamera( 
    fov, 
    window.innerWidth / window.innerHeight, 
    1, 
    10000
  );
  camera.position.x = 20;    
  camera.position.y = 10;    
  camera.position.z = 20;
  camera.lookAt(scene.position);
  scene.add(camera);
  
  var axis = new THREE.AxisHelper(10);
  scene.add (axis);
  
  material = new THREE.ShaderMaterial({
    uniforms: timeUniform,
    vertexShader: document.getElementById('vertex-shader').textContent,
    fragmentShader: document.getElementById('fragment-shader').textContent
  });
  
  var water = new THREE.Mesh(
    new THREE.PlaneBufferGeometry(window.innerWidth, window.innerHeight, 40), material
  );
  scene.add(water);
  
  var geometry = new THREE.SphereGeometry( 10, 32, 32 );
  var material = new THREE.MeshBasicMaterial( {color: 0xffff00} );
  var sphere = new THREE.Mesh( geometry, material );
  scene.add( sphere );

  renderer = new THREE.WebGLRenderer();
  renderer.setSize( window.innerWidth, window.innerHeight );
  
  container.appendChild( renderer.domElement );

  render();
});

window.addEventListener('resize',function() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize(window.innerWidth, window.innerHeight);
});

function render() {
  timeUniform.iGlobalTime.value += clock.getDelta();
  renderer.render(scene, camera);
  requestAnimationFrame(render);
}</script>
<!--动态背景天空结束。-->

    <div id="content">
        <div class="con">
            <div class="shlogo"></div>
            <div class="sou">
                <form action="" method="post" target="_self">
                    <input class="t" type="text" value="" name="t" hidden>
                    <input class="wd" type="text" placeholder="请输入搜索内容" name="q" x-webkit-speech lang="zh-CN">
                    <button><i style="font-size: 20px;" class="iconfont icon-sousuo"></i></button>
                </form>
                <ul>
                    <li data-s="baidu" target="_blank"><i style="background-image: url(icon/baidu.svg);"></i>百度一下</li>
                    <li data-s="google" target="_blank"><i style="background-image: url(icon/g.svg);"></i>Google</li>
                </ul>
            </div>
        </div>
       <div class="foot">© 2020-<?php echo date("Y") ?> by <a href="https://www.sunweihu.com/" target="_blank"><font color="red">墨涩网</font></a> ——基于开源项目二次开发.  <a rel="nofollow" href="https://5iux.github.io/sou/" target="_blank" style="font-size: 12px;"><i class="iconfont icon-github"></i></a></div>
    </div>
</body>
</html>